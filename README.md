#Lab. 2: Expresiones aritméticas (Quadratic Frog)

<div align='center'><img src="http://i.imgur.com/DJl6pYP.png" width="215" height="174">   <img src="http://i.imgur.com/c8bHQkh.png" width="215" height="174">   <img src="http://i.imgur.com/JUjEl5t.png" width="215" height="174"></div>

<p> </p>

Las expresiones aritméticas son parte esencial de casi cualquier algoritmo para resolver un problema útil.  Por lo tanto, aprender a implementar correctamente expresiones aritméticas es una destreza básica en cualquier lenguaje de programación. En esta experiencia de laboratorio practicarás la implementación de expresiones aritméticas en C++.

##Objetivos:

Al finalizar la experiencia de laboratorio de hoy los estudiantes habrán implementado expresiones aritméticas en C++ para definir la trayectoria del brinco de un sapito en un juego. También habrán aprendido a escribir e implementar un programa en donde practicarán el uso de constantes y escoger, declarar, convertir y usar el tipo adecuado para las variables.

##Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los siguientes conceptos:

	a. Implementación de expresiones aritméticas en C++

	b. Tipos de datos nativos de C++

	c. Uso del "type cast" para covertir el tipo de un número.

    d. Uso de funciones aritméticas de la biblioteca cmath

2. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

3. haber tomado el [quiz Pre-Lab 2](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=7113).


##Sesión de laboratorio:



###Fórmula cuadrática

Una *ecuación cuadrática* tiene la forma 

$$y = a x^2+ b x + c,$$

donde $a, b, c$ son números reales y $a\not=0$. La gráfica de una ecuación cuadrática es una *parábola* que abre hacia arriba si $a > 0$ y abre hacia abajo si $a < 0$. 

Una gráfica corta el eje de $x$ cuando $y=0$. Por lo tanto, si una parábola corta el eje de $x$, los cortes están dados por las soluciones reales de la ecuación

$$0 = a x^2 + b x + c.$$

Estas soluciones están dadas por la *fórmula cuadrática*

$$x=\frac{-b±\sqrt{b^2-4ac}}{2a}.$$

Nota que si el *discriminante*  $b^2-4ac$ de la fórmula cuadrática es negativo, los valores de $x$ serán números complejos y no se pueden graficar en el plano cartesiano porque los puntos en este plano tienen coordenadas reales. Por lo tanto, si el discriminante es negativo, la parábola no corta el eje de $x$. Si el discriminate es igual a $0$, entonces la parábola corta el eje de $x$ en un solo punto (solo el vértice toca el eje de $x$).

Si el discriminante es positivo, la fórmula cuadrática nos dá dos soluciones a la ecuación $0 = a x^2 + b x + c$ y estas soluciones son los cortes en el eje de $x$. Por ejemplo, supón que la fórmula cuadrática nos dá dos valores 

$$x=h_1$$
$$x=h_2$$

Entonces, 

$$a x^2 + b x + c = a(x-h_1)(x-h_2),$$


donde $h_1$ y $h_2$ son los cortes en el eje de $x$. Si $a<0$, la gráfica de la parábola será algo así

<div align='center'><img src="http://i.imgur.com/Zoq7guz.jpg" width="272" height="237" alt="parabola" /></div> <div align='center'><b>Figura 1.</b> Parábola que abre hacia abajo e intersepta el eje de $x$ en $h_1$ y $h_2$.</div>

Nota que la ecuación

$$y=-(x-h_1)(x-h_2)$$

es una ecuación cuadrática cuya parábola abra hacia abajo y corta el eje de $x$ en los puntos $h_1$ y $h_2$. Por ejemplo, la ecuación

$$y=-(x+2)(x-3)=-x^2+x+6$$

es una ecuación cuadrática cuya parábola abre hacia abajo y corta el eje de $x$ en los puntos $x=-2$ y $x=3$. Nota que, en esta ecuación, los valores de $a,b,c$  son $a=-1, \ b=1, \ c=6$.


###Ejercicio 1



En este ejercicio utilizarás la fórmula cuadrática para crear un juego en el que un sapito brinca de una hoja a otra. Vamos a asumir que las hojas están localizadas sobre el eje de $x$ y que el brinco del sapito estará determinado por una parábola que abre hacia abajo. Si queremos que el sapito brinque de hoja a hoja, debemos hayar una ecuación cuadrática cuya parábola abra hacia abajo y corte el eje de $x$ en los lugares donde están localizadas las hojas. Tu tarea es escribir las ecuaciones para la fórmula cuadrática.



**Instrucciones**

1.	Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/Lab02-QuadraticFrog-Expressions.git` para descargar la carpeta `Lab02-QuadraticFrog-Expressions` a tu computadora.

2.	Haz doble "click" en el archivo `QuadraticFrog.pro` para cargar el proyecto a Qt. Configura el proyecto y ejecuta el programa marcando el botón verde en el menú de la izquierda. El programa te dará un mensaje de que la fórmula cuadrática está mal escrita. Esto sucede porque el programa tiene instrucciones que prueban el programa y verifican que la implementación del código esté correcta. Tu programa aún no tiene las instrucciones para evaluar la fórmula cuadrática y por esto sale el mensaje. 

3. En el archivo `QuadraticFormula.cpp` (en Sources) escribirás las ecuaciones para la fórmula cuadrática. En la función `QuadraticPlus` añade la ecuación

 	$$result=\frac{-b+\sqrt{b^2-4ac}}{2a},$$

 	y en la función `QuadraticMinus` añade la ecuación

 	$$result=\frac{-b-\sqrt{b^2-4ac}}{2a},$$

 	Los otros archivos del proyecto tienen código que, evaluando las ecuaciones que escribiste con varios valores para $a, b ,c$ verificará si escribiste las ecuaciones correctamente. La validación del código y de los datos de entrada de un programa es una parte importante de el desarrollo de programas y proyectos.

4. Ejecuta el programa marcando el botón verde en el menú de la izquierda. Si las ecuaciones que escribiste están correctas, deberá aparecer una pantalla parecida a la pantalla de la Figura 2.


	<div align='center'><img src="http://i.imgur.com/b0dxEkK.png" width="272" height="237" alt="sapito" /></div>
    <div align='center'><b>Figura 2.</b> Pantalla del juego <i>Quadratic Frog</i>.</div>

	Al obtener las ecuaciones correctas, copia las expresiones para `result` que utilizaste en las funciones `QuadraticPlus` y `QuadraticMinus` y añádelas al lugar correspondiente del [enlace Entrega 1 del Lab 2](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=7117).

5. Para jugar, el sapito deberá brincar de una hoja a otra. Nota que las hojas tienen valores para $x_1$ y $x_2$. Estos valores representan los interceptos en el eje de $x$ de la parábola. En las cajas deberás entrar los valores para $a,b,c$ de modo que la gráfica de la ecuación cuadrática sea una parábola que abra hacia abajo e intercepte el eje de $x$ en los valores de las hojas. Puedes obtener los valores de $a,b,c$ notando que 

 $$a x^2 + b x + c = a(x-x_1)(x-x_2),$$

 como en la explicación de arriba.



###Ejercicio 2

Supón que cada curso en la Universidad de Yauco es de $3$ créditos y que las notas tienen las siguientes puntuaciones: $A = 4$ puntos; $B = 3$ puntos; $C = 2$ puntos; $D = 1$ punto y $F = 0$ puntos. 

Escribe un programa que defina las constantes $A=4, B=3, C=2, D=1, F=0$ para la puntuación de las notas y lea los valores para las variables $NumA$, $NumB$, $NumC$, $NumD$, $NumF$, donde $NumA$ representará el número de cursos en los que el estudiante obtuvo $A$, donde $NumB$ representará el número de cursos en los que el estudiante obtuvo $B$, etc. El programa debe desplegar el promedio general del estudiante.

**Ayuda:** recuerda que, en C++, si divides dos números enteros el resultado se "truncará" y será un número entero. Utiliza "type casting": `static_cast\<tipo\>(número)' para resolver este problema.

Verifica tu programa calculando el promedio de un estudiante que tenga dos A, dos B y una C. Cuando tu programa esté correcto, guarda el archivo y entrégalo  utilizando [este enlace a Moodle](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=7345).
