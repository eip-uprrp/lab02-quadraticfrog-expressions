#include "frog.h"
#include <QDebug>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"

frog::frog(QWidget *parent) :
    QWidget(parent)
{
    myTimer= new QTimer(this);
    flag = xFlag = false;
    Ysapo = 190;
    A = B = C = 0;
    temp = 580;
    temp2 = 0;
    backFlag = false;
    moved = false;
    textBool = false;
    buttonCounter = 0;
    xnenu = -(rand() %30);
    Xsapo = xnenu;
    initialX = Xsapo;
    xnenu2 = (rand() % 100) + 100;
    xnenu3 = xnenu2 + (rand() % 100) + 200;
}

void frog::updatenenu(){
    xnenu = -(rand() % 30);
    Xsapo = xnenu;
    initialX = Xsapo;
    xnenu2 = (rand() % 100) + 100;
    xnenu3 = xnenu2 + (rand() % 100) + 200;
}

void frog::reset(){
    flag = xFlag = false;
    Ysapo = 190;
    A = B = C = 0;
    temp = 580;
    temp2 = 0;
    backFlag = false;
    moved = false;
    buttonCounter = 0;
    textBool = false;
    repaint();

    myTimer->stop();
    delete myTimer;
    myTimer = new QTimer;
    connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));
    xnenu = -(rand() %30);
    Xsapo = xnenu;
    initialX = Xsapo;
    xnenu2 = (rand() % 100) + 100;
    xnenu3 = xnenu2 + (rand() % 100) + 200;

}

void frog::paintEvent(QPaintEvent *event){
    QPainter p(this);
    QString frogger = ":/images/Frogger1.png";
    QString frogger2 = ":/images/Frogger2.png";
    QString frogger3 = ":/images/Frogger3.png";
    QString leaf = ":/images/Nenufar1.png";
    QString back = ":/images/background.jpg";

//    Condiciones que permitian un rango de error (no caer en la x exacta de los nenufares)

    if(backFlag == true || (Xsapo <= (xnenu3 + 55) && Xsapo >= (xnenu3 - 55) && Ysapo == 190)
            || (Xsapo > xnenu && Ysapo == 190 && Xsapo < (xnenu2-55))
            || (Xsapo > xnenu2 && Ysapo == 190 && Xsapo < (xnenu3-55))
            || (Xsapo > xnenu3 && Ysapo == 190)){

//    if(backFlag == true || (Ysapo == 190 && (Xsapo != xnenu && Xsapo != xnenu2))){
        if (inter1Label == NULL){
            delete inter1Label;
        }
        if (inter2Label == NULL){
            delete inter2Label;
        }
        backFlag = true;
        myTimer->stop();
        delete myTimer;
        myTimer = new QTimer;
        QThread::msleep(5);
        connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));



        p.drawPixmap(temp2,-5,600,400,QPixmap(back));
        p.drawPixmap(temp,-5,600,400,QPixmap(back));

        p.drawPixmap(xnenu,250,125,60,QPixmap(leaf));
        p.drawPixmap(xnenu2,250,125,60,QPixmap(leaf));
        p.drawText(xnenu2+45,290,"x1 = " + QString::number(xnenu2));
        p.drawPixmap(xnenu3,250,125,60,QPixmap(leaf));
        p.drawText(xnenu3+45,290,"x2 = " + QString::number(xnenu3));



        Xsapo=Xsapo-1;
        xnenu=xnenu-1;
        xnenu2=xnenu2-1;
        xnenu3=xnenu3-1;
        temp= temp - 1;
        temp2 = temp2 -1;


        repaint();



        if(temp == 0 || xnenu3 == -10 || Xsapo == 0){
            backFlag = false;
            temp = 580;
            temp2 = 0;
            moved = true;
            textBool = false;
            updatenenu();
        }

    }
    else{
          QFont font = p.font();
          font.setBold(true);
          p.setFont(font);

          p.drawPixmap(temp2,-5,600,400,QPixmap(back));
          p.drawPixmap(temp,-5,600,400,QPixmap(back));
          p.drawPixmap(xnenu,250,125,60,QPixmap(leaf));
          p.drawPixmap(xnenu2,250,125,60,QPixmap(leaf));
          p.drawPixmap(xnenu3,250,125,60,QPixmap(leaf));

//          Cambiada la condicion para que sea exacta la caida en el nenufar
//          if(Xsapo < (xnenu2 + 55) && Ysapo == 190 && Xsapo > (xnenu2-55)){
//               textBool = true;
//          }

          if(Ysapo == 190 && Xsapo == xnenu2){
            textBool = true;
          }

          if(textBool){
              p.drawText(xnenu2+35,290,"x1 = " + QString::number(xnenu2));
              p.drawText(xnenu3+35,290,"x2 = " + QString::number(xnenu3));
              p.drawPixmap(xnenu,250,125,60,QPixmap(leaf));
          }
          else{
              p.drawText(xnenu2+35,290,"x2 = " + QString::number(xnenu2));
          }

    }

//    Cambiada la condicion para que sea exacta la caida en el nenufar
    if(Xsapo > (xnenu + 55) && Ysapo == 190 && Xsapo < (xnenu2-55)
        || (Xsapo > (xnenu2 + 55) && Ysapo == 190 && Xsapo < (xnenu3-55))
        || (Xsapo > (xnenu3 + 55) && Ysapo == 190)){

//    if(Ysapo == 190 && (Xsapo != xnenu && Xsapo != xnenu2 && Xsapo != xnenu3)){
        p.drawPixmap(Xsapo, Ysapo + 30, 100, 80, QPixmap(frogger3));
    }
    else if(Ysapo < 190){
        p.drawPixmap(Xsapo,Ysapo,100,100,QPixmap(frogger2));
    }else if(Ysapo == 190){
        qDebug() << "xsapo: " << Xsapo << "xnenu2: " << xnenu2;
        p.drawPixmap(Xsapo,Ysapo,100,100,QPixmap(frogger));
        if(!textBool){
            p.drawText(xnenu+45,290,"x1 = " + QString::number(xnenu+10));
        }
    }

}

frog::~frog(){
    delete myTimer;
}


void frog::mySlot(){
    float interX1 = 0;
    float interX2 = 0;
    float temp = 0;
    float y = 0;
    float xtemp = 0;
    float xEnd = 0;



    interX1 = QuadraticMinus(A,B,C);
    interX2 = QuadraticPlus(A,B,C);

    if(interX1 > interX2){
        temp = interX1;
        interX1 = interX2;
        interX2 = temp;
    }
    if(moved){
        moved = false;
        buttonCounter = 1;
        initialX = Xsapo;
    }

    xEnd = interX2 + initialX;
        y = 1;
        xtemp = 1;
        if(!xFlag){
            Ysapo = Ysapo - y;
            Xsapo = Xsapo + xtemp;
        }
        else if(xFlag){
            Ysapo = Ysapo + y;
            Xsapo = Xsapo + xtemp;
        }

        if(int(Xsapo) == int(xEnd/2)){
            xFlag = true;
        }

    repaint();
    if((int(Xsapo) == int(xEnd) && xFlag == true) || Ysapo == 190){
        Ysapo = 190;
        repaint();

        myTimer->stop();
        delete myTimer;
        myTimer = new QTimer;
        connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));

        initialX = Xsapo;
        xFlag = false;
    }

}

void frog::run(int aa, int bb, int cc){

    A = aa;
    B = bb;
    C = cc;
    buttonCounter++;

    connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));
    myTimer->start(25);
}
