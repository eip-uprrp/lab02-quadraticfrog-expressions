///
/// Modifications:
///     - 2015-01-13 [RAN] - Changed the quadraticFormulaCheck function to
///                 prevent giving away the expression

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    quadraticFormulaCheck();


    ui->setupUi(this);
    scene= new QGraphicsScene(this);
    scene->setSceneRect(QRectF(QPointF(0,0), QPointF(0,0)));
    ui->MainView->setScene(scene);
    ui->MainView->setAlignment((Qt::AlignLeft | Qt::AlignTop));
    ui->MainView->resize(580,345);
    ui->MainView->move(0,0);

    ui->alabel->move(10,360);
    ui->alineEdit->move(25,358);
    ui->blabel->move(85,360);
    ui->blineEdit->move(100,358);
    ui->clabel->move(160,360);
    ui->clineEdit->move(175,358);
    ui->RunPushButton->move(10,385);
    ui->retryButton->move(180,385);



    frogger = new frog;
    //ui->x1Label->setText(fr);
   // ui->x1Label->move(frogger->xnenu2, 300);
    globalTimer = new QTimer;
    scene->addWidget(frogger);

    ui->RunPushButton->setEnabled(false);

}


MainWindow::~MainWindow()
{
    delete ui;
    delete scene;
}

void MainWindow::quadraticFormulaCheck()
{
    // float firstX;
    float resultFromFunction;
    bool pass = true;
    float values[] = {357 , -1000 , 698 , 1.4804781099 , 748 , -392 , 51 , 0.28391805554 ,
        689 , -689 , 172 , 0.519048482944 , 90 , 521 , 751 , -2.71178575308 , 5 , 107 ,
        465 , -6.0642692054 , 1609 , -983 , 150 , 0.314734649792 , 273 , 496 , 224 ,
        -0.839700867988 , 2 , 91 , 920 , -15.1630045473 , 48 , 300 , 463 , -2.77889067238 ,
        852 , 907 , 241 , -0.510947439149};

    float valuesForMinus[] = {129 , -486 , 456 , 1.76744186047 , 384 , 980 , 625 , -1.30208333333 ,
        270 , -904 , 755 , 1.59515823795 , 67 , -450 , 752 , 3.12650485528 , 98 , 506 , 651 ,
        -2.72985550047 , 38 , -373 , 901 , 4.29396930804 , 273 , -282 , 72 , 0.461538461538 ,
        225 , -889 , 874 , 1.84 , 8 , 136 , 522 , -11.1457513111 , 5 , 77 , 214 , -11.760788100};

    float a, b, c, expected;

    //Verifying if the equation written by the student is correct
    for (int i = 0; i < 40 && pass; i+=4){
        a        = values[i];
        b        = values[i+1];
        c        = values[i+2];
        expected = values[i+3];

        // firstX = (((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a));
        resultFromFunction = frogger->QuadraticPlus(a,b,c);

        if( abs(resultFromFunction - expected) > 0.00001) {
            pass = false;
        }
    }
    if(!pass){
        QMessageBox::information(this, "Error", "La funcion cuadratica esta mal escrita!");
        exit(1);
    }

    //Verifying if the equation written by the student is correct
    for (int i = 0; i < 40 && pass; i+=4){
        a        = valuesForMinus[i];
        b        = valuesForMinus[i+1];
        c        = valuesForMinus[i+2];
        expected = valuesForMinus[i+3];

        // firstX = (((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a));
        resultFromFunction = frogger->QuadraticMinus(a,b,c);

        if( abs(resultFromFunction - expected) > 0.00001) {
            pass = false;
        }
    }
    if(!pass){
        QMessageBox::information(this, "Error", "La funcion cuadratica esta mal escrita!");
        exit(1);
    }
}

void MainWindow::on_RunPushButton_clicked()
{
    int a = 0;
    int b = 0;
    int c = 0;
    int det = 0;
    a = (ui->alineEdit->text().toInt());
    b = (ui->blineEdit->text().toInt());
    c = (ui->clineEdit->text().toInt());

    det = (pow(b,2) - (4*a*c));

    if(a > 0){
        QMessageBox::information(this, "Error", "La parabola abre hacia arriba y por lo tanto el sapo no brincara");
        ui->alineEdit->clear();
        ui->blineEdit->clear();
        ui->clineEdit->clear();
    }
    else if(a == 0){
        QMessageBox::information(this, "Error", "No hay parabola y por lo tanto el sapo no brincara");
        ui->alineEdit->clear();
        ui->blineEdit->clear();
        ui->clineEdit->clear();
    }
    else{
        if(det < 0){
            QMessageBox::information(this, "Error", "No tiene intercepto y por lo tanto el sapo no brincara");
            ui->alineEdit->clear();
            ui->blineEdit->clear();
            ui->clineEdit->clear();
        }
        else if(det == 0){
            QMessageBox::information(this, "Error", "El vertice esta en el intercepto y por lo tanto el sapo no brincara");
            ui->alineEdit->clear();
            ui->blineEdit->clear();
            ui->clineEdit->clear();
        }
        else{
            frogger->run(a, b, c);
            ui->RunPushButton->setEnabled(false);
            ui->alineEdit->clear();
            ui->blineEdit->clear();
            ui->clineEdit->clear();
        }
    }


}

void MainWindow::on_retryButton_clicked()
{
    frogger->reset();
}

void MainWindow::on_alineEdit_textChanged(const QString &arg1)
{
    if(ui->alineEdit->text() != "" && ui->blineEdit->text() != "" && ui->clineEdit->text() != "" ){
        ui->RunPushButton->setEnabled(true);
    }
    else{
        ui->RunPushButton->setEnabled(false);
    }
}

void MainWindow::on_blineEdit_textChanged(const QString &arg1)
{
    if(ui->alineEdit->text() != "" && ui->blineEdit->text() != "" && ui->clineEdit->text() != "" ){
        ui->RunPushButton->setEnabled(true);
    }
    else{
        ui->RunPushButton->setEnabled(false);
    }
}



void MainWindow::on_clineEdit_textChanged(const QString &arg1)
{
    if(ui->alineEdit->text() != "" && ui->blineEdit->text() != "" && ui->clineEdit->text() != "" ){
        ui->RunPushButton->setEnabled(true);
    }
    else{
        ui->RunPushButton->setEnabled(false);
    }
}
