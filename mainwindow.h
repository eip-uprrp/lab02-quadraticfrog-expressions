 #ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include "frog.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void quadraticFormulaCheck();

private slots:
    void on_RunPushButton_clicked();

    void on_retryButton_clicked();

    void on_alineEdit_textChanged(const QString &arg1);

    void on_blineEdit_textChanged(const QString &arg1);

    void on_clineEdit_textChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    QGraphicsScene*scene;
    QTimer *globalTimer;
    frog *frogger;
};

#endif // MAINWINDOW_H
