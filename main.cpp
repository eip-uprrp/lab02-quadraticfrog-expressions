#include <QApplication>
#include "mainwindow.h"
#include <cstdlib>
#include <ctime>

int main(int argc, char *argv[])
{
    srand(time(NULL));
    QApplication a(argc, argv);
    MainWindow w;
    w.resize(580,455);
    w.show();

    return a.exec();
}
