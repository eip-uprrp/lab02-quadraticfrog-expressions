#-------------------------------------------------
#
# Project created by QtCreator 2014-07-22T10:58:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QuadraticFrog2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    frog.cpp \
    QuadraticFormula.cpp

HEADERS  += mainwindow.h \
    frog.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources/images.qrc
