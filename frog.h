#ifndef FROG_H
#define FROG_H

#include <QWidget>
#include <QPainter>
#include <QtGui>
#include <QtCore>
#include <QTimer>
#include <QLabel>

class frog : public QWidget
{
    Q_OBJECT
public:
    explicit frog(QWidget *parent = 0);
    float Ysapo, Xsapo, initialX;
    bool flag, xFlag, backFlag, moved, textBool;
    int A, B, C, buttonCounter,temp,temp2,xnenu,xnenu2,xnenu3;

    float QuadraticPlus(int a, int b, int c);
    float QuadraticMinus(int a, int b, int c);
    ~frog();

private:
    QLabel *inter1Label;
    QLabel *inter2Label;

signals:

public slots:
    void mySlot();
    void run(int, int, int);
    void reset();
    void updatenenu();

protected:
    void paintEvent(QPaintEvent *event);
    QTimer *myTimer;

};

#endif // FROG_H
